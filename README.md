
### Development mode

To start the Figwheel compiler, navigate to the project folder and run the following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser.
Once Figwheel starts up, you should be able to open the `public/index.html` page in the browser.


### Prod build and deploy

```
lein clean
lein package
TMPDIR=`mktemp -d`
cp -r public ${TMPDIR}
rm -r ${TMPDIR}/public/js/release
CACHE_BUSTER=$(md5 < public/js/app.js | cut -b 1-10)
cp -v ${TMPDIR}/public/js/app.js ${TMPDIR}/public/js/app.${CACHE_BUSTER}.js
sed -i "" -e s/app.js/app.${CACHE_BUSTER}.js/ ${TMPDIR}/public/index.html
aws s3 sync ${TMPDIR} s3://optifuel-52c1b041e042ab/
open d1jxcavdziwa8e.cloudfront.net
```
