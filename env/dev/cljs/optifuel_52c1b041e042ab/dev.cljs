(ns ^:figwheel-no-load optifuel-52c1b041e042ab.dev
  (:require
    [optifuel-52c1b041e042ab.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
