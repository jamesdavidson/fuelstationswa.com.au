(ns optifuel-52c1b041e042ab.prod
  (:require
    [optifuel-52c1b041e042ab.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
