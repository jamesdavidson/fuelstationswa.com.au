(ns optifuel-52c1b041e042ab.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]
                   [optifuel-52c1b041e042ab.macros.data :refer [fuelwatch-data-url]])
  (:require
    [cljs.core.async :refer [put! chan <! >! timeout close! onto-chan]]
    [goog.net.XhrIo] [goog.date.DateTime] [goog.date.Interval]
    [reagent.core :as r]))

(def MAPBOX_TOKEN "pk.eyJ1IjoiamFtZXNkYXZpZHNvbiIsImEiOiIwNENwNms0In0._WiFUK3qkE4O7Sne-pl9Gw")
(def MAPBOX_BASEURL "https://api.mapbox.com")

(defn squared-distance
 [a b]
 (reduce
  +
  (map #(js/Math.pow % 2)
   (vals
    (merge-with
     -
     (select-keys a [:LONGITUDE :LATITUDE])
     (select-keys b [:LONGITUDE :LATITUDE]))))))

(def stringify-coordinates
 #(->> %
   (map (juxt :LONGITUDE :LATITUDE))
   (map (partial clojure.string/join ","))
   (clojure.string/join ";")))

(defonce state (r/atom {
 ::product "ULP"
 ::ticker " "
 ::lowest :DURATION
}))

(defonce ticker
 (let [ch (chan)]
  (go-loop []
   (when-let [tick (<! ch)]
    (swap! state assoc ::ticker tick)
    (<! (timeout 1000))
    (recur)))
  ch))

(defn get-json
 [url]
 (let [ch (chan)]
  (onto-chan ticker ["." ".." "..." " "] false)
  (goog.net.XhrIo/send url
   (fn [e]
    (if-let [data (-> e .-target .getResponseText js/JSON.parse js->clj clojure.walk/keywordize-keys)]
     (put! ch data))
    (close! ch)))
   ch))

(defn get-current-position
 []
 (let [ch (chan)]
  (if js/navigator.geolocation
   (js/navigator.geolocation.getCurrentPosition
    (fn [p]
     (let [lon (-> p .-coords .-longitude)
           lat (-> p .-coords .-latitude)]
      (put! ch {:LONGITUDE lon :LATITUDE lat})))
    (fn [e]
     (do (swap! state assoc ::geolocation-failed true) (js/console.error e) (close! ch))))
   (close! ch))
  ch))

(defn get-address-coordinates
 [address]
 (let [ch (chan)]
  (go
   (->>
    (<! (get-json (str MAPBOX_BASEURL
                       "/geocoding/v5/mapbox.places/"
                       (js/encodeURIComponent address)
                       ".json?country=AU&type=poi"
                       "&access_token=" MAPBOX_TOKEN)))
    :features
    (filter #(< 0.5 (:relevance %)))
    first
    :geometry
    :coordinates
    (interleave [:LONGITUDE :LATITUDE])
    (apply hash-map)
    (put! ch)))
  ch))

(defn get-distances
 [here stations]
 (let [ch (chan)]
  (go
   (->>
    (<! (get-json (str MAPBOX_BASEURL
                       "/directions-matrix/v1/mapbox/driving/"
                       (stringify-coordinates (cons here stations))
                       "?annotations=distance,duration"
                       "&sources=0"
                       "&access_token=" MAPBOX_TOKEN)))
    :durations
    first
    (drop 1)
    (map #(assoc %1 :DURATION %2) stations)
    (put! ch)))
  ch))

(def qwerty
 #(vector :p
    (:PRODUCT_DESCRIPTION %) " @ "
    "$" (.toFixed (* 0.01 (js/Number (:PRODUCT_PRICE %))) 2) [:br]
    " ~" (.toFixed (* (/ 1.0 60) (:DURATION %)) 0) " minutes drive away"
    ", " (:TRADING_NAME %) [:br]
    (let [address (str (:ADDRESS %) " " (:LOCATION %) " " (:POSTCODE %))]
     [:a {:href (str "https://maps.apple.com/maps?q=" (js/encodeURIComponent address))}
      address])))

(def today
 (let [x (new goog.date.DateTime)]
  (.add x (new goog.date.Interval goog.date.Interval.HOURS -6))
  (-> x (.toIsoString true) (.substring 0 10))))

(js/setTimeout
 (fn [_]
  (when (empty? (::data @state))
   (goog.net.XhrIo/send (fuelwatch-data-url)
    (fn [e] (->> e .-target .getResponseText
                 clojure.string/split-lines
                 (map #(js/JSON.parse %))
                 (map js->clj)
                 (map #(dissoc % ""))
                 clojure.walk/keywordize-keys
                 (filter #(= today (:date %)))
                 (into #{})
                 (swap! state assoc ::data)))))))

(defn home-page []
  [:div.container
    [:div {:style {:margin "20px 0px"}}
     [:h2 "Fuel Stations WA"]
     [:p "Find cheap fuel in Western Australia with daily price data provided by "
      [:a {:href "https://www.fuelwatch.wa.gov.au"} "FuelWatch"]
      " for "
      (.toLocaleDateString (new js/Date today) "en-AU" #js{:weekday "long", :day "numeric", :month "long", :year "numeric"})
      "."]]
    [:div.form-group
     [:select.form-control
      {:value (-> @state ::product)
       :on-change #(do (swap! state assoc ::product (-> % .-target .-value))
                       (swap! state dissoc ::results))}
      [:option "ULP"]
      [:option "98 RON"]
      [:option "Brand Diesel"]
      [:option "LPG"]
      [:option "Diesel"]
      [:option "E85"]
      [:option "PULP"]]]

    (if (::geolocation-failed @state)
     [:div
      [:div.form-group
       [:p "Hmm, geolocation failed."]
       [:p "Would you like to try searching for a street address instead?"]
       [:input.form-control
        {:placeholder "i.e. 231 Stirling Highway, Claremont"
         :type "text"
         :on-change (fn [e] (swap! state assoc ::address (-> e .-target .-value)))}]]])

    [:div.form-group
     [:button.btn.btn-primary.btn-lg
      {:type "button"
       :style {:width "100%"}
       :on-click (fn [_]
                  (go
                   (if-let [here (<! (if (::geolocation-failed @state)
                                         (get-address-coordinates (::address @state))
                                         (get-current-position)))]
                    (let [{:optifuel-52c1b041e042ab.core/keys [data address product]} @state
                          stations (->> data
                                    (sort-by (partial squared-distance here))
                                    (filter #(= product (:PRODUCT_DESCRIPTION %)))
                                    (take 10))]
                     (if-let [results (<! (get-distances here stations))]
                      (swap! state assoc ::results results))))))}
       (str "Search for " (-> @state ::product) " fuel")]]
    (if-not (empty? (::results @state))
     [:div.row
      [:div {:class "btn-group btn-group-toggle" :style {:margin "auto"}}
       [:label {:class (if (= :DURATION (::lowest @state)) ["btn" "btn-secondary" "active"] ["btn" "btn-secondary"])}
        [:input {:type "radio"
                 :on-click (fn [_] (swap! state assoc ::lowest :DURATION))
                 :checked (= :DURATION (::lowest @state))}] "Nearest"]
       [:label {:class (if (= :PRODUCT_PRICE (::lowest @state)) ["btn" "btn-secondary" "active"] ["btn" "btn-secondary"])}
        [:input {:type "radio"
                 :on-click (fn [_] (swap! state assoc ::lowest :PRODUCT_PRICE))
                 :checked (= :PRODUCT_PRICE (::lowest @state))}] "Cheapest"]]]
     [:div.container [:p {:style {:text-align "center" :font-size "72px"}} (::ticker @state)]])
     [:div {:style {:min-height "10px"}}] ; spacer
     (into [:div] (->> @state ::results (filter :DURATION) (sort-by (::lowest @state)) (map qwerty)))])

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
