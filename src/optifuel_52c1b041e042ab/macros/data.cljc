(ns optifuel-52c1b041e042ab.macros.data
 #?@(:clj  [(:require clojure.data.xml) (:require clojure.data.json)]))

(defn get-xml [a]
 #?(:clj  (clojure.data.xml/parse (.openStream (new java.net.URL a)))))

(def get-xml' (memoize get-xml))

(defn md5 [s]
 #?(:clj
    (apply str
     (map
      #(.substring (Integer/toString (+ 256 (bit-and % 255)) 16) 1)
      (.digest
       (java.security.MessageDigest/getInstance "MD5")
       (byte-array (map byte s)))))))

(def fuelwatch-product-codes {
 "ULP" 1
 "PULP" 2
 "Diesel" 4
 "LPG" 5
 "98 RON" 6
 "E85" 10
 "Brand diesel" 11
})

(defn item->map [{:keys [content]}]
 (clojure.set/rename-keys
  (into {}
   (map #(vector (:tag %) (first (:content %))) content))
  {:latitude "LATITUDE"
   :longitude "LONGITUDE"
   :price "PRODUCT_PRICE"
   :address "ADDRESS"
   :location "LOCATION"
   :trading-name "TRADING_NAME"}))

(def stringified-fuelwatch-data
 #?(:clj
  (doall
   (apply str
    (apply concat
     (for [[name code] fuelwatch-product-codes]
      (->> (str "https://www.fuelwatch.wa.gov.au/fuelwatch/fuelWatchRSS?Product=" code)
       get-xml'
       :content first :content (filter #(= :item (:tag %)))
       (map item->map)
       (map #(assoc % "PRODUCT_DESCRIPTION" name))
       (map #(str (clojure.data.json/write-str %) \newline)))))))))

(defmacro fuelwatch-data-url []
  #?(:clj  (let [filename (str (md5 stringified-fuelwatch-data) ".json")]
            (spit (str "public/" filename) stringified-fuelwatch-data)
            (str "/" filename))
     :cljs (do "/037b62206aca5877b66994d54bd2ebf9.json")))
